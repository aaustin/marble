// SPDX-License-Identifier: LGPL-2.1-or-later
//
// SPDX-FileCopyrightText: 2011 Utku Aydın <utkuaydin34@gmail.com>
//

#ifndef MARBLE_GEOSCENETYPES_H
#define MARBLE_GEOSCENETYPES_H

namespace Marble
{

namespace GeoSceneTypes
{
/**
 * Please keep alphabetic order to prevent mess
 */
extern const char GeoSceneDocumentType[];
extern const char GeoSceneFilterType[];
extern const char GeoSceneGeodataType[];
extern const char GeoSceneGroupType[];
extern const char GeoSceneHeadType[];
extern const char GeoSceneIconType[];
extern const char GeoSceneItemType[];
extern const char GeoSceneLayerType[];
extern const char GeoSceneLegendType[];
extern const char GeoSceneLicenseType[];
extern const char GeoSceneMapType[];
extern const char GeoScenePaletteType[];
extern const char GeoScenePropertyType[];
extern const char GeoSceneSectionType[];
extern const char GeoSceneSettingsType[];
extern const char GeoSceneTextureTileType[];
extern const char GeoSceneTileDatasetType[];
extern const char GeoSceneVectorType[];
extern const char GeoSceneVectorTileType[];
extern const char GeoSceneXmlDataSourceType[];
extern const char GeoSceneZoomType[];
}

}

#endif
